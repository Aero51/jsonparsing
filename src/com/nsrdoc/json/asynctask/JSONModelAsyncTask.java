package com.nsrdoc.json.asynctask;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.ParseException;
import android.os.AsyncTask;
import android.util.Log;

import com.nsrdoc.json.callbacks.TaskModelCallbacks;
import com.nsrdoc.json.model.Model;

public class JSONModelAsyncTask extends AsyncTask<String, Integer, Boolean> {
	private static final String TAG = "JSONModelAsyncTask";
	private static final boolean DEBUG = true; // Set this to false to disable
	private ArrayList<Model> modelList;
	private TaskModelCallbacks mCallback;

	public JSONModelAsyncTask(ArrayList<Model> modelList, TaskModelCallbacks callback) {
		this.modelList = modelList;
		mCallback = callback;

		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onPreExecute() {
		mCallback.onPreExecute();
		// Proxy the call to the Activity.
		if (DEBUG)
			Log.d("Test", "on pre execute");

	}

	@Override
	protected Boolean doInBackground(String... urls) {
		try {

			HttpGet httppost = new HttpGet(urls[0]);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httppost);

			int status = response.getStatusLine().getStatusCode();

			if (status == 200) {
				HttpEntity entity = response.getEntity();
				String data = EntityUtils.toString(entity);

				JSONArray jarray = new JSONArray(data);

				for (int i = 0; i < jarray.length(); i++) {
					JSONObject object = jarray.getJSONObject(i);
					Model model = new Model();
					model.setUserId(object.getString("userId"));
					model.setId(object.getString("id"));
					model.setTitle(object.getString("title"));
					modelList.add(model);
				}
				return true;
			}

		} catch (ParseException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	protected void onPostExecute(Boolean result) {
		mCallback.onPostExecute(modelList, result);
		if (DEBUG)
			Log.d(TAG, "Loading finished: " + result);

	}

	@Override
	protected void onCancelled() {
		// Proxy the call to the Activity.
		mCallback.onCancelled();

	}

}