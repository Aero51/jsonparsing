package com.nsrdoc.json.asynctask;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.ParseException;
import android.os.AsyncTask;
import android.util.Log;

import com.nsrdoc.json.callbacks.TaskPhotosModelCallbacks;
import com.nsrdoc.json.model.PhotosModel;

public class JSONPhotosModelAsyncTask extends AsyncTask<String, Void, Boolean> {

	private static final String TAG = "JSONPhotosModelAsyncTask";
	private static final boolean DEBUG = true; // Set this to false to disable

	private ArrayList<PhotosModel> photosModelList;
	private TaskPhotosModelCallbacks mCallback;

	public JSONPhotosModelAsyncTask(ArrayList<PhotosModel> photosModelList,
			TaskPhotosModelCallbacks callback) {
		this.photosModelList = photosModelList;
		this.mCallback = callback;
	}

	@Override
	protected void onPreExecute() {
		mCallback.onPreExecute();
		// Proxy the call to the Activity.
		if (DEBUG)
			Log.d("Test", "on pre execute ");

	}

	@Override
	protected Boolean doInBackground(String... urls) {
		try {

			// ------------------>>
			HttpGet httppost = new HttpGet(urls[0]);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httppost);

			// StatusLine stat = response.getStatusLine();
			int status = response.getStatusLine().getStatusCode();

			if (status == 200) {
				HttpEntity entity = response.getEntity();
				String data = EntityUtils.toString(entity);

				JSONArray jarray = new JSONArray(data);

				for (int i = 0; i < jarray.length(); i++) {
					JSONObject object = jarray.getJSONObject(i);

					PhotosModel photosmodel = new PhotosModel();

					photosmodel.setAlbumId(object.getString("albumId"));
					photosmodel.setId(object.getString("id"));
					photosmodel.setTitle(object.getString("title"));
					photosmodel.setThumbnailUrl(object
							.getString("thumbnailUrl"));

					photosModelList.add(photosmodel);
				}
				return true;
			}

			// ------------------>>

		} catch (ParseException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	protected void onPostExecute(Boolean result) {
		mCallback.onPostExecute(photosModelList, result);
		if (DEBUG)
			Log.d(TAG, "Loading finished: " + result);
		if (result == false) {
		}

	}
	@Override
	protected void onCancelled() {
		// Proxy the call to the Activity.
		mCallback.onCancelled();

	}
}
