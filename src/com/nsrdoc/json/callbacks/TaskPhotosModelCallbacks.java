package com.nsrdoc.json.callbacks;

import java.util.ArrayList;

import com.nsrdoc.json.model.PhotosModel;



public interface TaskPhotosModelCallbacks {
	
	void onCancelled();
	void onPreExecute();

	void onPostExecute(ArrayList<PhotosModel> photosModelList, boolean result);

}
