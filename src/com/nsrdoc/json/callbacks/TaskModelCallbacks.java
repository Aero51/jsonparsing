package com.nsrdoc.json.callbacks;

import java.util.ArrayList;

import com.nsrdoc.json.model.Model;

public interface TaskModelCallbacks {

	void onCancelled();
	void onPreExecute();

	void onPostExecute(ArrayList<Model> modelList, boolean result);
}
