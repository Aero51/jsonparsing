package com.nsrdoc.json.adapters;

import java.util.ArrayList;

import com.nsrdoc.json.R;
import com.nsrdoc.json.R.drawable;
import com.nsrdoc.json.R.id;
import com.nsrdoc.json.model.PhotosModel;
import com.nsrdoc.json.utils.DrawableBackgroundDownloader;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PhotosAdapter extends ArrayAdapter<PhotosModel> {

	private static String  baseUrl="https://placeholdit.imgix.net/~text?txtsize=14&bg=";
	private static String  endUrl="&txt=150%C3%97150&w=150&h=150";
	
	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();
	private Context mContext;
	ArrayList<PhotosModel> photosModelList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;

	public PhotosAdapter(Context context, int resource,
			ArrayList<PhotosModel> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mContext = context;
		Resource = resource;
		photosModelList = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);

			holder.image= (ImageView) v.findViewById(R.id.picture);
			holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
			//holder.image.setLayoutParams(new ListView.LayoutParams(150, 150));

			holder.albumId = (TextView) v.findViewById(R.id.tvalbumId);
			holder.id = (TextView) v.findViewById(R.id.tvid);
			holder.title = (TextView) v.findViewById(R.id.tvtitle);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		Drawable drawable = mContext.getResources().getDrawable(
				R.drawable.photo_not_available);
		Log.d("photosAdapter", "thumbnail url:"
				+ photosModelList.get(position).getThumbnailUrl());
		String url=photosModelList.get(position).getThumbnailUrl();
		String picId=url.substring(url.length()-6);
		
		Log.d("photosAdapter", "thumbnail item:"
				+ picId);
		
		downloader.loadDrawable(baseUrl+picId+endUrl, holder.image,drawable);
		//downloader.loadDrawable("https://placeholdit.imgix.net/~text?txtsize=14&bg=9ed3d5&txt=150%C3%97150&w=150&h=150", holder.image,drawable);
	

		holder.albumId.setText(photosModelList.get(position).getAlbumId());
		holder.id.setText(photosModelList.get(position).getId());
		holder.title.setText(photosModelList.get(position).getTitle());

		return v;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (photosModelList != null) {
			return photosModelList.size();
		} else {
			return 0;
		}

	}

	static class ViewHolder {

		public ImageView image;
		public TextView albumId;
		public TextView id;
		public TextView title;

	}

}