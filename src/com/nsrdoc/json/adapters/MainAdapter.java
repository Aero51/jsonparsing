package com.nsrdoc.json.adapters;

import java.util.ArrayList;





import com.nsrdoc.json.R;
import com.nsrdoc.json.R.id;
import com.nsrdoc.json.model.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MainAdapter extends ArrayAdapter<Model> {
	ArrayList<Model> modelList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;

	public MainAdapter(Context context, int resource, ArrayList<Model> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		modelList = objects;
	}
 
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			
			holder.userId = (TextView) v.findViewById(R.id.tvuserId);
			holder.id = (TextView) v.findViewById(R.id.tvid);
			holder.title = (TextView) v.findViewById(R.id.tvdescription);
			
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		
		holder.userId.setText(modelList.get(position).getUserId());
		holder.id.setText(modelList.get(position).getId());
		holder.title.setText(modelList.get(position).getTitle());
	
		return v;

	}

	static class ViewHolder {
		
		public TextView userId;
		public TextView id;
		public TextView title;
		

	}


}