package com.nsrdoc.json.headlessFragments;

import java.util.ArrayList;

import com.nsrdoc.json.activities.PhotosActivity;
import com.nsrdoc.json.asynctask.JSONPhotosModelAsyncTask;
import com.nsrdoc.json.callbacks.TaskPhotosModelCallbacks;
import com.nsrdoc.json.model.Model;
import com.nsrdoc.json.model.PhotosModel;

import de.greenrobot.event.EventBus;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

public class TaskPhotosModelFragment extends Fragment {

	private static final String TAG = TaskPhotosModelFragment.class
			.getSimpleName();
	private static final boolean DEBUG = true; // Set this to false to disable
	private static String BASE_URL = "http://jsonplaceholder.typicode.com/albums/";
	private static String EXTENSION = "/photos";

	private Model model;
	private TaskPhotosModelCallbacks mCallbacks;
	private JSONPhotosModelAsyncTask jsonPhotosModelTask;
	private ArrayList<PhotosModel> photosModelList;

	public ArrayList<PhotosModel> getPhotosModelList() {
		return photosModelList;
	}

	@Override
	public void onAttach(Activity activity) {
		if (DEBUG)
			Log.i(TAG, "onAttach(Activity)");
		super.onAttach(activity);
		if (!(activity instanceof TaskPhotosModelCallbacks)) {
			throw new IllegalStateException(
					"Activity must implement the TaskCallbacks interface.");
		}

		// Hold a reference to the parent Activity so we can report back the
		// task's results.
		mCallbacks = (TaskPhotosModelCallbacks) activity;
		model = (Model) EventBus.getDefault().getStickyEvent(Model.class);

	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		if (DEBUG)
			Log.i(TAG, "onCreate(Bundle)");
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		if (getActivity().getClass().equals(PhotosActivity.class)) {
			if (photosModelList == null) {
				if (DEBUG)
					Log.d(TAG, "List is null");
				photosModelList = new ArrayList<PhotosModel>();
				startJSONPhotosModelAsyncTask();
			}
		}
	}

	@Override
	public void onDestroy() {
		if (DEBUG)
			Log.i(TAG, "onDestroy()");
		super.onDestroy();
		cancelJSONPhotosModelAsyncTask();
		;
	}

	public void startJSONPhotosModelAsyncTask() {

		if (DEBUG)
			Log.i(TAG, "start()");

		jsonPhotosModelTask = new JSONPhotosModelAsyncTask(photosModelList,
				mCallbacks);
		jsonPhotosModelTask.execute(BASE_URL + model.getId() + EXTENSION);
	}

	/**
	 * Cancel the background task.
	 */
	public void cancelJSONPhotosModelAsyncTask() {
		Log.d(TAG, "Async Task cancelled!");
		jsonPhotosModelTask.cancel(false);
		jsonPhotosModelTask = null;

	}

}
