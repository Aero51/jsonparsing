package com.nsrdoc.json.headlessFragments;

import java.util.ArrayList;

import com.nsrdoc.json.activities.MainActivity;
import com.nsrdoc.json.asynctask.JSONModelAsyncTask;
import com.nsrdoc.json.callbacks.TaskModelCallbacks;
import com.nsrdoc.json.model.Model;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * TaskFragment manages a single background task and retains itself across
 * configuration changes.
 */
public class TaskModelFragment extends Fragment {
	private static final String TAG = TaskModelFragment.class.getSimpleName();
	private static final boolean DEBUG = true; // Set this to false to disable
	private static final String BASE_URL = "http://jsonplaceholder.typicode.com/albums"; // logs.

	private ArrayList<Model> modelList;

	/*public void setModelList(ArrayList<Model> modelList) {
		this.modelList = modelList;
	}*/

	public ArrayList<Model> getModelList() {
		return modelList;
	}

	private TaskModelCallbacks mCallbacks;
	private JSONModelAsyncTask jsonModelTask;

	/**
	 * Hold a reference to the parent Activity so we can report the task's
	 * results. The Android framework will pass us a reference to the newly
	 * created Activity after each configuration change.
	 */
	@Override
	public void onAttach(Activity activity) {
		if (DEBUG)
			Log.i(TAG, "onAttach(Activity)");
		super.onAttach(activity);
		if (!(activity instanceof TaskModelCallbacks)) {
			throw new IllegalStateException(
					"Activity must implement the TaskCallbacks interface.");
		}

		// Hold a reference to the parent Activity so we can report back the
		// task's results.
		mCallbacks = (TaskModelCallbacks) activity;
	}

	/**
	 * This method is called once when the Fragment is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (DEBUG)
			Log.i(TAG, "onCreate(Bundle)");
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		if (getActivity().getClass().equals(MainActivity.class)) {
			if (modelList == null) {
				if (DEBUG)
					Log.d(TAG, "List is null");
				modelList = new ArrayList<Model>();
				startJSONModelAsyncTask();

			}

		}

	}

	/**
	 * Note that this method is <em>not</em> called when the Fragment is being
	 * retained across Activity instances. It will, however, be called when its
	 * parent Activity is being destroyed for good (such as when the user clicks
	 * the back button, etc.).
	 */
	@Override
	public void onDestroy() {
		if (DEBUG)
			Log.i(TAG, "onDestroy()");
		super.onDestroy();
		cancelJSONModelAsyncTask();
	}

	/*****************************/
	/***** TASK FRAGMENT API *****/
	/*****************************/

	/**
	 * Start the background task.
	 */
	public void startJSONModelAsyncTask() {

		if (DEBUG)
			Log.i(TAG, "start()");

		jsonModelTask = new JSONModelAsyncTask(modelList, mCallbacks);
		jsonModelTask.execute(BASE_URL);
	}

	/**
	 * Cancel the background task.
	 */
	public void cancelJSONModelAsyncTask() {
		Log.d(TAG, "Async Task cancelled!");
		jsonModelTask.cancel(false);
		jsonModelTask = null;

	}

}
