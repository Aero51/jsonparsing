package com.nsrdoc.json.activities;

import java.util.ArrayList;

import com.nsrdoc.json.R;
import com.nsrdoc.json.adapters.PhotosAdapter;
import com.nsrdoc.json.callbacks.TaskPhotosModelCallbacks;
import com.nsrdoc.json.headlessFragments.TaskPhotosModelFragment;
import com.nsrdoc.json.model.PhotosModel;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

public class PhotosActivity extends FragmentActivity implements TaskPhotosModelCallbacks {
	
	
	private static final String TAG = PhotosActivity.class.getSimpleName();
	private static final boolean DEBUG = true; // Set this to false to disable logs											
	private static final String TAG_TASK_PHOTOS_MODEL_FRAGMENT = "task_photos_model_fragment";
	
	private ArrayList<PhotosModel> photosModelList;
	private ListView listview;
	private PhotosAdapter adapter;
	private ProgressDialog dialog;
	private TaskPhotosModelFragment mFragment;
	private boolean mRunning = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (DEBUG)
			Log.i(TAG, "onCreate(Bundle)");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		 getActionBar().setDisplayHomeAsUpEnabled(true);

		 listview = (ListView) findViewById(R.id.photoslist);
		 
			FragmentManager fm = getSupportFragmentManager();
			mFragment = (TaskPhotosModelFragment) fm.findFragmentByTag(TAG_TASK_PHOTOS_MODEL_FRAGMENT);
			if (mFragment == null) {
				if (DEBUG)
					Log.d(TAG, "Creating new task fragment");
				mFragment = new TaskPhotosModelFragment();
				fm.beginTransaction().add(mFragment, TAG_TASK_PHOTOS_MODEL_FRAGMENT)
						.commit();
			}

			if (mRunning) {
				mFragment.cancelJSONPhotosModelAsyncTask();
			} else {
				if (mFragment.getPhotosModelList() != null) {

					if (DEBUG)
						Log.d(TAG, "List is not null");
					photosModelList = mFragment.getPhotosModelList();
					setAdapter();
				}
			}

	}

	private void showProgressDialog() {
		dialog = new ProgressDialog(PhotosActivity.this);
		dialog.setMessage("Loading, please wait");
		dialog.setTitle("Connecting server");
		dialog.show();
		dialog.setCancelable(false);
		dialog.show();

	}
	private void setAdapter() {
		adapter = new PhotosAdapter(getApplicationContext(),
				R.layout.detailrow, photosModelList);

		listview.setAdapter(adapter);

	}
	
	 @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        int id = item.getItemId();
	        if (id == android.R.id.home) {
	          
	            finish();
	            return true;
	        }
	        return super.onOptionsItemSelected(item);
	    }
	
	
	/*********************************/
	/*****PHOTOS TASK CALLBACK METHODS *****/
	/*********************************/
	
	@Override
	public void onCancelled() {
		if (DEBUG)
			Log.i(TAG, "onCancelled()");
		Toast.makeText(this, R.string.task_cancelled_msg, Toast.LENGTH_SHORT)
				.show();
		
	}
	@Override
	public void onPreExecute() {
		showProgressDialog();
		mRunning = true;
		
	}
	@Override
	public void onPostExecute(ArrayList<PhotosModel> photosModelList,
			boolean result) {
		mRunning = false;
		dialog.cancel();
		if (result == true) {
			this.photosModelList = photosModelList;
			setAdapter();
		} else {
			Toast.makeText(getApplicationContext(),
					"Unable to fetch data from server", Toast.LENGTH_LONG)
					.show();
			if (DEBUG)
				Log.d(TAG, "on post execute: Unable to fetch data from server");
		}
		
	}
}
