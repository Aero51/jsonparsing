package com.nsrdoc.json.activities;

import java.util.ArrayList;

import com.nsrdoc.json.R;
import com.nsrdoc.json.adapters.MainAdapter;
import com.nsrdoc.json.callbacks.TaskModelCallbacks;
import com.nsrdoc.json.headlessFragments.TaskModelFragment;
import com.nsrdoc.json.model.Model;

import de.greenrobot.event.EventBus;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements TaskModelCallbacks,AdapterView.OnItemClickListener{

	private static final String TAG = MainActivity.class.getSimpleName();
	private static final boolean DEBUG = true; // Set this to false to disable
												// logs
	private static final String TAG_TASK_MODEL_FRAGMENT = "task_model_fragment";

	private TaskModelFragment mTaskFragment;
	private ArrayList<Model> modelList;
	private boolean mRunning = false;

	private MainAdapter adapter;
	private ListView listview;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (DEBUG)
			Log.i(TAG, "onCreate(Bundle)");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		listview = (ListView) findViewById(R.id.list);
		listview.setOnItemClickListener(this);

		FragmentManager fm = getSupportFragmentManager();
		mTaskFragment = (TaskModelFragment) fm.findFragmentByTag(TAG_TASK_MODEL_FRAGMENT);
		if (mTaskFragment == null) {
			if (DEBUG)
				Log.d(TAG, "Creating new task fragment");
			mTaskFragment = new TaskModelFragment();
			fm.beginTransaction().add(mTaskFragment, TAG_TASK_MODEL_FRAGMENT)
					.commit();
		}

		if (mRunning) {
			mTaskFragment.cancelJSONModelAsyncTask();
		} else {
			if (mTaskFragment.getModelList() != null) {

				if (DEBUG)
					Log.d(TAG, "List is not null");
				modelList = mTaskFragment.getModelList();
				setAdapter();
			}
		}
	}

	/*********************************/
	/***** TASK CALLBACK METHODS *****/
	/*********************************/

	@Override
	public void onPreExecute() {
		// TODO Auto-generated method stub
		showProgressDialog();
		mRunning = true;

	}

	@Override
	public void onCancelled() {
		if (DEBUG)
			Log.i(TAG, "onCancelled()");
		Toast.makeText(this, R.string.task_cancelled_msg, Toast.LENGTH_SHORT)
				.show();
	}

	@Override
	public void onPostExecute(ArrayList<Model> modelList, boolean result) {
		mRunning = false;
		dialog.cancel();
		if (result == true) {
			this.modelList = modelList;
			setAdapter();
		} else {
			Toast.makeText(getApplicationContext(),
					"Unable to fetch data from server", Toast.LENGTH_LONG)
					.show();
			if (DEBUG)
				Log.d(TAG, "on post execute: Unable to fetch data from server");
		}
	}

	private void launchPhotosActivity(int position) {
		Intent newActivity = new Intent(MainActivity.this, PhotosActivity.class);
		EventBus.getDefault().postSticky(modelList.get(position));
		startActivity(newActivity);
		// finish();
	}

	private void setAdapter() {
		adapter = new MainAdapter(getApplicationContext(), R.layout.mainrow,
				modelList);
		listview.setAdapter(adapter);

	}

	private void showProgressDialog() {
		dialog = new ProgressDialog(MainActivity.this);
		dialog.setMessage("Loading, please wait");
		dialog.setTitle("Connecting server");
		dialog.show();
		dialog.setCancelable(false);
		dialog.show();

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (DEBUG)
			Log.d(TAG, "on item click position :" + position);
		launchPhotosActivity(position);
		
		
	}

}
